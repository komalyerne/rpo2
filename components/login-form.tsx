import { Formik, Field, Form, FormikHelpers } from 'formik';
import styles from './login-form.module.css'
import router, { useRouter } from 'next/router'

interface Values {
    username: string;
    password: string;
}

export default function LoginForm() {
  // const router=useRouter()

  // const handleClick= () => {
  //   console.log('Logged in')
  //   router.push('/ProductListPage')
  // }
    return (
      <div className={styles.login_box + ' p-3'}>
        <h1 className="display-6 mb-3">Login</h1>
            
          

         
        <Formik
          initialValues={{
            username: '',
            password: '',
            
          }}

          onSubmit={(
            values: Values,
            { setSubmitting }: FormikHelpers<Values>
          ) => {
            setTimeout(() => {
              alert(JSON.stringify(values, null, 2));
              setSubmitting(false);
            }, 500);
            <button type="button" onClick={() => router.push('/ProductListPage')}>Login</button>
          }}

        >
          <Form>
            <div className="mb-3">
              <Field className="form-control" id="username" name="username" placeholder="Username" aria-describedby="usernameHelp" />
            </div>
  
            <div className="mb-3">
              <Field className="form-control" id="password" name="password" placeholder="Password" type="password" />
            </div>

            {/* <button type="submit" className="btn btn-primary">Login</button> */}
          <button onClick={handleClick}></button>
          </Form>
        </Formik>
      </div>
    );
  };
